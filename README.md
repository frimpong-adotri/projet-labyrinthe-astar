# Projet Labyrinthe AStar

Ce mini-projet vise à illustrer le fonctionnement de l'algorithme A*  pour la recherche d'un court chemin dans un labyrinthe.  Il a été réalisé dans le cadre d'une évaluation de 3ème année de licence informatique à l'Université Paris Cité.

## Informations générales
Les fichiers contenus dans ce document constituent le rendu final du projet d'algorithmie sujet 1 portant sur le labyrinthe.
Le document se composent de deux scripts Python.

### Sujet: 
Sujet 1 - Projet Labyrinthe

### Auteur: 
Frimpong ADOTRI

### Langage : 
Python


## Modules_labyrinthe.py

Ce fichier est composé de plusieurs fonctions, méthodes et classes utilitaires nécessaires pour modéliser et resoudre le problème de recherche du plus court chemin pour sortir du labyrinthe. Ce fichier utilise aussi des modules Python prédéfinis parmi lesquelles on retrouve: 

**- numpy :** module de calcul scientifique basique. C'est la méthode 'sqrt()' de ce module qui est utilisé dans ce fichier pour calculer une racine carrée (cf. distance euclidienne).

**- heapq :** classe implémentant une file de priorité. Il a un comportement similaire à la classe générique 'PriorityQueue<>()' en Java.

**- time :** module qui calcule des durées. Ici, c'est la méthode 'time()' qui est utilisé pour calculer le temps d'exécution du programme
    

Après avoir énuméré ces modules Pythons utilisés, voici les classes et méthodes écrites dans ce fichier (synthaxe Python):


    creerLabyrinthe(fichier:str) -> list:
    """
    Cette méthode créé et affiche un labyrinthe à partir d'un fichier texte

    Parameters
    ----------
    fichier : str
        DESCRIPTION. chemin d'accès au fichier texte contenant l'architecture du labyrinthe à construire

    Returns
    -------
    list
        DESCRIPTION. La liste contenant l'architecture du labyrinthe à construire
    """


    resolution_labyrinthe(fichier:str) -> None:
    
    """
    Cette méthode simule la recherche du plus court chemin de sortie d'un labyrinthe contenu dans un fichier texte passé en argument

    Parameters
    ----------
    fichier : str
        DESCRIPTION. fichier texte contenant l'architecture d'un labyrinthe

    Returns
    -------
    None
        DESCRIPTION.

    """


    class  Chemin_labyrinthe(object):
    
    """
    Classe destinée à contenir les outils nécessaires pour déterminer le plus court chemin pour sortir du labyrinthe.
    Il se compose essentiellement d'un constructeur, d'une heurique et d'un algorithme A*

    

    __init__(self, labyrinthe:list, position_debut:str, sortie:str) -> None:
        """
        Cette méthode est le constructeur de notre classe qui recherche le plus court chemin entre la position initiale du damné et la sortie du labyrinthe
        
        Parameters
        ----------
        labyrinthe : list
            DESCRIPTION. Il s'agit d'un labyrinthe codé comme une liste de liste de chaines de caractères
        position_debut : str
            DESCRIPTION. Il indique le caractère marquant la position de début du damné dans le labyrinthe
        sortie : str
            DESCRIPTION. Il indique le caractère marquant la sortie du labyrinthe

        Returns
        -------
        None.

        """



    recuperer_coordonnees(self, position:str) -> tuple :
        """
        Cette méthode récupère les coordonnées d'un caractère passé en argument

        Parameters
        ----------
        position : str
            DESCRIPTION. caractère modélisant la position dont on souhaite connaitre

        Returns
        -------
        tuple 
            DESCRIPTION. Le tuple correspondant aux coordonnées voulus

        """



    recuper_distance(self, chemin:list) -> int:
        """
        Cette méthode retourne la longueur de la liste passée en argument

        Parameters
        ----------
        chemin : list
            DESCRIPTION. Liste contenant l'ensemble des points de passage du chemin solution

        Returns
        -------
        int
            DESCRIPTION. Longueur de la liste contenant les points de passages solutions

        """



    
    heuristique(self, coordonnees_position:tuple) -> float:
        """
        Cette méthode a pour rôle de calculer l'heuristique d'un point dans le labyrinthe. Ici on utilise plutôt un calcul de distance euclidienne plutôt qu'un calcul de distance de Manhattan

        Parameters
        ----------
        coordonnees_position : tuple
            DESCRIPTION. Coordonnées d'un point dans le labyrinthe

        Returns
        -------
        float
            DESCRIPTION. Heuristique du point correspondant à sa distance euclidienne par rapport au point de sortie

        """





    prochain_mouvement(self, position_precedente:tuple) -> tuple:
        """
        Cette méthode est une méthode génératrice (ou générateur). Elle génère un itérable suivant un modèle de tuple de directions Haut, Bas, Gauche, Droite

        Parameters
        ----------
        position_precedente : tuple
            DESCRIPTION. Ce tuple correspond aux coordonnées de la position à partir de laquelle, on souhaite rechercher le prochain déplacement approprié

        Yields
        ------
        tuple
            DESCRIPTION.  itérateur correspondant aux configurations en coordonnées des directions Haut, Bas, Gauche, Droite à partir des coordonnées du point passés en argument

        """





    AStar(self)->list:
        """
        Cette méthode est une implémentation de l'algorithme A* avec l'heuristique définie plus haut

        Returns
        -------
        list
            DESCRIPTION. La liste contenant les points de passage du chemin solution. Si cette liste est vide,l'algorithme n'a pas trouvé de chemin de sortie du labyrinthe 

        """





    chemin_solution(self, solution:list) -> list:
        """
        Cette méthode créé une liste contenant la configuration du labyrinthe initial avec les points de passage du chemin solution marqués

        Parameters
        ----------
        solution : list
            DESCRIPTION. La liste contenant les points de passage du chemin solution 

        Returns
        -------
        list
            DESCRIPTION. Liste contenant la configuration du labyrinthe en prenant en soin de marquer les points de passages du chemin solution par un symbole '£'

        """
        

## Application_labyrinthe.py

Ce fichier contient l'interface utilisateur qui reprend les méthodes définies dans Modules_labyrinthe.py pour les mettre en oeuvre afin de produire un résultat visuel pour l'utilsateur. Il peut être comparé à la classe contenant la méthode main() en Java.

L'utilisateur se voit proposé un menu de deux options:

**- L'option 1** permet à l'utilisateur de visualiser un exemple d'application de l'algorithme sur un labyrinthe d'essai prédéfini
    
**- L'option 2** emmène l'utilisateur à saisir manuellement le chemin d'accès à un fichier texte contenant l'architecture d'un labyrinthe quelconque. Le programme met ensuite en oeuvre l'algorithme et les différents modules nécessaires pour trouver dans la mesure du possible le plus court chemin pour sortir du labyrinthe et le lui affiche à l'écran


# Informations pratiques GitLab

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/goldmalcom43/projet-labyrinthe-astar.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/goldmalcom43/projet-labyrinthe-astar/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
