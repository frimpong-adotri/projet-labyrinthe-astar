#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 13/12/2021

@author: Frimpong ADOTRI
"""

import Modules_labyrinthe



if __name__ == '__main__': 
    
    print("-------------------------------------------------------------+ Bienvenue +-------------------------------------------------------------")
    print("Choisissez une option : ")
    print("(1) Pour voir des résultats tests sur des exemples de labyrinthes")
    print("(2) Pour voir les résultats de l'algorithme sur un fichier texte de labyrinthe de votre choix")
    reponse = int(input("Entrez votre réponse : "))
    while (reponse not in (1,2)):
        print("Choix incorrect, veuillez réessayer !!!")
        print("Choisissez une option : ")
        print("(1) Pour voir des résultats tests sur des exemples de labyrinthes")
        print("(2) Pour voir les résultats de l'algorithme sur un fichier texte de labyrinthe de votre choix")
        reponse = int(input("Entrez votre réponse : "))
        
    if reponse == 1:
        print("\n\n")
        Modules_labyrinthe.resolution_labyrinthe("modeles_labyrinthes/labyrinthe.txt");
        print("\n\nAU REVOIR !!! ")
    elif reponse == 2:
        fichier:str = input("Veuillez entrer le chemin vers le fichier labyrinthe: ")
        Modules_labyrinthe.resolution_labyrinthe(fichier);
        print("\n\n")
        print("\nAU REVOIR !!!")
            
            
   
    
    
    